﻿using UnityEngine;
using System.Collections;

public class SoundButton : MonoBehaviour {

	public AudioClip buttonPush;
	
	AudioSource source;
	
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void playButtonPush()
	{
		source.PlayOneShot (buttonPush);
	}
}
