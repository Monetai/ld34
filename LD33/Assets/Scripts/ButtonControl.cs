﻿using UnityEngine;
using System.Collections;

public class ButtonControl : MonoBehaviour {

	public WallMovement wall;
	public CameraEffect effect;
	Animator ator;
	public ButtonWall buttonGroup;
	public bool iSPush;
	int player;

	// Use this for initialization
	void Start () {
		ator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Player1")
		{
			player =1;
			//effect.playAnim(CameraEffect.Effect.zoomOnAction);
			ator.Play("Push");
		}
		else if(col.gameObject.tag == "Player2")
		{
			player =2;
			//effect.playAnim(CameraEffect.Effect.zoomOnAction);
			ator.Play("Push");
		}
	}

	void push()
	{
		iSPush = true;

		if(player ==1)
		{
			wall.wallMouvementRight(5);
		}
		if(player ==2)
		{
			wall.wallMouvementLeft(5);
		}

		buttonGroup.moveToNextPos();
	}

	void dePush()
	{
		print ("prout");
		iSPush = false;
	}
}
