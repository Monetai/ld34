﻿using UnityEngine;
using System.Collections;

public class MainMenuHandler : MonoBehaviour {
	

	public GameObject voletB;
	public GameObject voletS;
	public float delay1;

	public GameObject sister,brother;
	public float delay2;

	public GameObject titleB;
	public float delay3;

	public GameObject titleVS;
	public float delay4;

	public GameObject titleS;
	public float delay5;

	public GameObject volet2,volet3;
	public GameObject combat1,combat2;
	public float delay6;


	public void load(string name)
	{
		Application.LoadLevel (name);
	}

	public void quit()
	{
		Application.Quit ();
	}

	// Update is called once per frame
	public void play () {
		StartCoroutine(anim());
	}

	IEnumerator anim() {
		voletB.SetActive (true);
		voletS.SetActive (true);

		yield return new WaitForSeconds(delay1);

		sister.SetActive (true);
		brother.SetActive (true);

		yield return new WaitForSeconds(delay2);

		titleB.SetActive (true);

		yield return new WaitForSeconds(delay3);

		titleVS.SetActive (true);
		
		yield return new WaitForSeconds(delay4);

		titleS.SetActive (true);

		yield return new WaitForSeconds(delay5);

		volet2.SetActive (true);
		volet3.SetActive (true);

		sister.SetActive (false);
		brother.SetActive (false);

		yield return new WaitForSeconds(0.30f);
		combat1.SetActive (true);
		combat2.SetActive (true);

		yield return new WaitForSeconds(delay6);

		Application.LoadLevel ("MainScene");
	}
}
