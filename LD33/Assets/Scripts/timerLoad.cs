﻿using UnityEngine;
using System.Collections;

public class timerLoad : MonoBehaviour {

	public string toLoad;
	public float timer;
	float time;
		
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;

		if (time >= timer) {
			Application.LoadLevel(toLoad);
		}
	}
}
