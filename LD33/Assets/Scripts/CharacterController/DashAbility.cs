﻿using UnityEngine;
using System.Collections.Generic;


public class DashAbility : MonoBehaviour {
	
	 DashState dashState;
	 float dashTimer = 0;
	public float cooldown = 20f;
	 float cooldownTimer = 0;
	public float speed = 1f;
	
	 Vector2 savedVelocity;
	public float strengh = 100f;
	public float dashDuration;
	 float timer;

	Rigidbody2D rBody;
	Animator ator;

	public string Fire_Input = "Fire1_P1";

	void Start()
	{
		rBody = GetComponent<Rigidbody2D>();
		ator = GetComponent<Animator>();
	}
	
	void Update () 
	{
		switch (dashState) 
		{
		case DashState.Ready:
			var isDashKeyDown = Input.GetButtonDown (Fire_Input);
			if(isDashKeyDown)
			{
				ator.Play("attaque");
				savedVelocity = rBody.velocity;
				//rBody.velocity =  new Vector2(rBody.velocity.x * 3f, rBody.velocity.y);
				dashState = DashState.Dashing;
			}
			break;
		case DashState.Dashing:

			dashTimer += Time.deltaTime;

			if(dashTimer >= dashDuration)
			{
				rBody.velocity = savedVelocity;
				rBody.velocity = Vector2.zero;
				dashTimer = 0;
				dashState = DashState.Cooldown;
			}
			else
			{
				if(savedVelocity.x >0)
				transform.position = Vector3.Lerp (transform.position,
				                                   new Vector3(transform.forward.x + strengh,transform.forward.y,transform.forward.z),
				                                   Time.deltaTime * speed);

				if(savedVelocity.x <0)
				transform.position = Vector3.Lerp (transform.position,
					                               new Vector3(-(transform.forward.x + strengh),transform.forward.y,transform.forward.z),
					                               Time.deltaTime * speed);
			}

			break;
		case DashState.Cooldown:
			cooldownTimer += Time.deltaTime;
			if(cooldownTimer >= cooldown)
			{
				cooldownTimer = 0;
				dashState = DashState.Ready;
			}
			break;
		}
	}
}

public enum DashState 
{
	Ready,
	Dashing,
	Cooldown
}