﻿using UnityEngine;
using System.Collections;

/*
 * TODO:
 * Debugger le saut
 * 	-le personnage ralenti lorsque l'on appuye sur une direction lors d'un saut
 * 	-ajouter sauts analogiques
 */

public class PlayerContoller : MonoBehaviour 
{
	public float maxGroundSpeed = 10f;
	public float maxAirSpeed = 10f;


	[Range(0.0f, 1.0f)]
	public float friction= 0.75f;

	public float accelRate = 35f;

	public float jumpSpeed = 8f;

	public float jumpDur = 2f;
	public bool canWallJump = true;


	public string horizontal_Input = "Horizontal_P1";
	public string jump_Input = "Jump_P1";

	bool facingRight = true;

	
	bool grounded;
	bool touchingWall;
	//bool canJump = true;
	bool jumpKeyDown = false;
	bool variableJump;
	float jumpDuration;


	float rayLenght = 0.1f;
	float colliderTreshold = 0.001f;

	Rigidbody2D rBody;
	SpriteRenderer spriteRend;
	Collider2D col2D;

	
	void Start () 
	{
		rBody = GetComponent<Rigidbody2D>();
		spriteRend = GetComponent<SpriteRenderer>();
		col2D = GetComponent<Collider2D>();

	}
	
	void FixedUpdate () 
	{
		float horizontal = Input.GetAxis(horizontal_Input);
		grounded = isOnGround();

		if(grounded && horizontal == 0)
		{
			rBody.velocity = new Vector2(0f,rBody.velocity.y);
		}
		else if( grounded)
		{
			moveHorizontal(maxGroundSpeed,horizontal);
		}
		else
		{
			moveHorizontal(maxAirSpeed,horizontal);
		}


		if(Input.GetButton(jump_Input))
		{
			if(!jumpKeyDown) // on the first frame
			{
				jumpKeyDown = true;

				if(grounded || canWallJump)
				{
					touchingWall = false;
					int hitDirection = 0;

					bool leftWallHit = isOnWallLeft();
					bool rightWallHit = isOnWallRight();

					//if(horizontal != 0)
					//{
						if(leftWallHit)
						{
							touchingWall = true;
							hitDirection = 1;
						}else if(rightWallHit)
						{
							touchingWall = true;
							hitDirection = -1;
						}
					//}

					if(!touchingWall)
					{
						if(grounded)
						{
							rBody.AddForce (new Vector2(this.rBody.velocity.x,this.jumpSpeed),ForceMode2D.Impulse);
							jumpDuration = 0f;
							variableJump = true;
						}
					}
					else
					{
						if(!grounded)
						{
							rBody.AddForce( new Vector2(this.rBody.velocity.x * hitDirection* jumpSpeed, this.jumpSpeed),ForceMode2D.Impulse);

							jumpDuration = 0f;
							variableJump = true;
						}
						else
						{
							rBody.AddForce( new Vector2(rBody.velocity.x, this.jumpSpeed),ForceMode2D.Impulse);
							jumpDuration = 0f;
							variableJump = true;
						}
					}
				}
			}// on the second frame
			else if(variableJump)
			{
				jumpDuration += Time.deltaTime;

				if(jumpDuration < this.jumpDur && Input.GetButton(jump_Input))
				{
	
					rBody.AddForce( new Vector2(rBody.velocity.x, this.jumpSpeed),ForceMode2D.Impulse);

				}
			}
		}
		else
		{
			jumpKeyDown = false;
			variableJump = false;
		}

		if (this.rBody.velocity.x > 0.1 && !facingRight)
		{
			Flip();
		}
		else if (this.rBody.velocity.x < -0.1 && facingRight)
		{
			Flip();
		}
	
	}
	
	void Update()
	{

	}

	private bool isOnGround()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x,
		                                this.col2D.bounds.center.y - this.col2D.bounds.extents.y - colliderTreshold);

		Vector2 rayTarget = new Vector2(lineStart.x, lineStart.y - this.rayLenght);


		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.blue,0);

		return ray;
	}

	private bool isOnWallLeft()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x - this.col2D.bounds.extents.x - colliderTreshold,
		                                this.col2D.bounds.center.y);

		Vector2 rayTarget = new Vector2(lineStart.x - this.rayLenght, lineStart.y);
		
		
		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.red,0);
		
		return ray;
	}

	private bool isOnWallRight()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x + this.col2D.bounds.extents.x + colliderTreshold,
		                                this.col2D.bounds.center.y);

		Vector2 rayTarget = new Vector2(lineStart.x + this.rayLenght, lineStart.y);
		
		
		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.red,0);
		
		return ray;
	}

	public void moveHorizontal(float speed, float input)
	{
		if(input < -0.1f)
		{
			if(rBody.velocity.x > -speed)
			{
				rBody.AddForce(new Vector2(-this.accelRate,0f));
			}
			else
			{
				rBody.velocity = new Vector2(-speed,rBody.velocity.y);
			}
		}
		else if(input > 0.1f)
		{
			if(rBody.velocity.x < speed)
			{
				rBody.AddForce(new Vector2(this.accelRate,0f));
			}
			else
			{
				rBody.velocity = new Vector2(speed,rBody.velocity.y);
			}
		}
	}

	public void  OnDrawGizmos()
	{

	}
	
	void Flip()
	{      
		facingRight = !facingRight;
		
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}
}