﻿using UnityEngine;
using System.Collections;

public class BetterPlayerController : MonoBehaviour {

	/*
	 * TODO:
	 * slide up a reparer
	 * timing du sticky aussi
	 * 
	 */

	enum State
	{
		Grounded,
		Aerial,
		Sliding,
		Sticking
	}

	//variables relatives a la vitesse de deplacement
	public float maxSpeed = 10f;
	public float maxAirSpeed = 10f;
	public float groundAceleration = 10f;
	public float airAcceleration = 8f;
	public float slideSpeed = 2f;
	public float stickyTime = 1f;
	public float jumpStrenght = 3f;
	public float jumpDuration = 2f;
	public bool checkLeft = true;
	public bool checkRight = true;

	//Gestion des Inputs
	public string horizontal_Input = "Horizontal_P1";
	public string jump_Input = "Jump_P1";
	public string attack_Input = "Fire1_P1  sGrounded  ";
	float horizontal;
	bool jump;
	bool canJump;
	bool attacking;

	//variable pour le retournement du gameObject
	bool facingRight = true;

	//Variable pour les raycasts
	float rayLenght = 0.05f;
	float colliderTreshold = 0.001f;

	//variables d'etats
	bool isGrounded;
	bool isTouchingWall;
	bool touchLeftWall;
	bool touchRightWall;
	bool isStiking = false;
	float stickTimer;
	float jumpTimer;
	int wall;
	State state;
	bool hasJump = false;
	bool hasStick = false;

	//composants physique
	Rigidbody2D rBody;
	Collider2D col2D;

	Animator ator;


	// Use this for initialization
	void Start () {
	
		rBody = GetComponent<Rigidbody2D>();
		col2D = GetComponent<Collider2D>();
		ator = GetComponent<Animator>();
	}

	void Update()
	{
		horizontal = Input.GetAxis(horizontal_Input);
		jump = Input.GetButton(jump_Input);
		attacking = Input.GetButton(attack_Input);

		if(horizontal !=0)
			ator.SetBool("isMoving",true);
		else
			ator.SetBool("isMoving",false);
	}
	
	// Update is called once per frame
	void setState ()
	{
		isTouchingWall = touchWall();
		isGrounded = isOnGround();

		ator.SetBool("isGrounded",isGrounded);
		ator.SetBool("onWall",isTouchingWall);
		
		if(isGrounded)
		{
			state = State.Grounded;
		}
		else if( !isGrounded)
		{
			state = State.Aerial;
		}

		if(state == State.Aerial && isTouchingWall)
		{

			if(isStiking)
			{
				state = State.Sticking;
			}
			else if (horizontal != 0)
			{
				state =State.Sliding;
				wall = witchWall();
			}
		}
	}

	void FixedUpdate () 
	{
		setState();

		switch(state)
		{

		case State.Aerial:
				
				performVariableJump();
				moveHorizontal(maxAirSpeed,airAcceleration,horizontal);
				/*if(attacking && canAttack)
				{
					canAttack = false;
					Attack();
				}*/
			
			break;

		case State.Grounded:

				canJump = true;
				hasJump = false;
				if(horizontal == 0)
					rBody.velocity = new Vector2(0f,rBody.velocity.y);

				if(jump && canJump)
				{

					jumpTimer = 0;
					rBody.AddForce( new Vector2(0f, jumpStrenght),ForceMode2D.Impulse);
				}
				
				moveHorizontal(maxSpeed,groundAceleration,horizontal);
				
				/*if(attacking && canAttack)
				{
					canAttack = false;
					Attack();
				}*/

			break;

		case State.Sliding:
				canJump = true;
				jumpTimer = 0;
				if(jump && canJump)
				{
					hasStick=false;
					jumpTimer = 0;
					rBody.velocity = new Vector2(jumpStrenght * wall, jumpStrenght*2);
					hasJump= true;
				}
				
				if(rBody.velocity.y > 0 && !hasJump)
				{
					rBody.AddForce( new Vector2(0f, -rBody.velocity.x),ForceMode2D.Impulse);
				}
				
				if(wall == 1)
				{
					if(horizontal != 0 && horizontal < wall && rBody.velocity.y < -slideSpeed)
					{
						rBody.velocity = new Vector2(0f, -slideSpeed);
					}
					else if( horizontal != 0 && horizontal >= wall && !hasStick)
					{
						ator.SetBool("isSticky",true);
						stickTimer = 0;
						isStiking = true;
					}
				}else if(wall == -1)
				{
					if(horizontal != 0 && horizontal > wall && rBody.velocity.y < -slideSpeed)
					{
						rBody.velocity = new Vector2(0f, -slideSpeed);
					}
					else if( horizontal != 0 && horizontal <= wall && !hasStick)
					{
						ator.SetBool("isSticky",true);
						stickTimer = 0;
						isStiking = true;
					}
				}

			break;

		case State.Sticking:
				hasStick=true;
				stickTimer += Time.fixedDeltaTime;

				if(stickTimer < stickyTime)
				{
					//rBody.velocity = new Vector2(0f, 0f);

					if(jump && canJump)
					{
						hasStick=false;
						jumpTimer = 0;
						rBody.velocity = new Vector2(jumpStrenght* wall, jumpStrenght);
						hasJump = true;
					}
				}
				else
				{
					rBody.AddForce( new Vector2(jumpStrenght* wall, rBody.velocity.y),ForceMode2D.Impulse);
					isStiking = false;
					ator.SetBool("isSticky",false);
				}
			break;

		}

		if (this.rBody.velocity.x > 0.1 && !facingRight)
		{
			Flip();
		}
		else if (this.rBody.velocity.x < -0.1 && facingRight)
		{
			Flip();
		}

	}

	int witchWall()
	{
		if(isOnWallLeft())
		{
			return 1;
		}
		else if(isOnWallRight())
		{
			return -1;
		}
		else return 0;
	}

	bool touchWall()
	{
		bool right = false;
		bool left = false;

		if(checkLeft)
		left = isOnWallLeft();

		if(checkRight)
		right = isOnWallRight();

		if(left || right)
			return true;

		return false;
	}

	public void performVariableJump()
	{
		jumpTimer += Time.fixedDeltaTime;
		
		if(jumpTimer < jumpDuration && Input.GetButton(jump_Input) && canJump)
		{
			rBody.AddForce( new Vector2(0f, jumpStrenght),ForceMode2D.Impulse);
			
		}
		else
		{
			canJump= false;
		}
	}

	/*
	 *Move the gameobject on the X axis
	 */
	public void moveHorizontal(float maxSpeed, float accel, float input)
	{
		if(input < -0.1f)
		{
			if(rBody.velocity.x > -maxSpeed)
			{
				rBody.AddForce(new Vector2(-accel,0f));
			}
			else
			{
				rBody.velocity = new Vector2(-maxSpeed,rBody.velocity.y);
			}
		}
		else if(input > 0.1f)
		{
			if(rBody.velocity.x < maxSpeed)
			{
				rBody.AddForce(new Vector2(accel,0f));
			}
			else
			{
				rBody.velocity = new Vector2(maxSpeed,rBody.velocity.y);
			}
		}
	}


	/*
	 public void moveHorizontal(float maxSpeed, float accel, float input)
	{
		if(input < -0.1f)
		{
			if(rBody.velocity.x > -maxSpeed)
			{
				rBody.AddForce(new Vector2(-accel,0f));
			}
			else
			{
				rBody.velocity = new Vector2(-maxSpeed,rBody.velocity.y);
			}
		}
		else if(input > 0.1f)
		{
			if(rBody.velocity.x < maxSpeed)
			{
				rBody.AddForce(new Vector2(accel,0f));
			}
			else
			{
				rBody.velocity = new Vector2(maxSpeed,rBody.velocity.y);
			}
		}
	}
	*/

	/*
	 *Verification de la position par rapport au sol
	 */
	private bool isOnGround()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x,
		                                this.col2D.bounds.center.y - this.col2D.bounds.extents.y - colliderTreshold);
		
		Vector2 rayTarget = new Vector2(lineStart.x, lineStart.y - this.rayLenght);
		
		
		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.blue,0);
		
		return ray;
	}

	/*
	 *Verification de la position par rapport au mur gauche
	 */
	private bool isOnWallLeft()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x - this.col2D.bounds.extents.x - colliderTreshold,
		                                this.col2D.bounds.center.y);
		
		Vector2 rayTarget = new Vector2(lineStart.x - this.rayLenght, lineStart.y);
		
		
		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.red,0);
		
		return ray;
	}


	/*
	 *Verification de la position par rapport au mur droit
	 */
	private bool isOnWallRight()
	{
		Vector2 lineStart = new Vector2(this.col2D.bounds.center.x + this.col2D.bounds.extents.x + colliderTreshold,
		                                this.col2D.bounds.center.y);
		
		Vector2 rayTarget = new Vector2(lineStart.x + this.rayLenght, lineStart.y);
		
		
		RaycastHit2D ray = Physics2D.Linecast(lineStart,rayTarget);
		Debug.DrawLine(lineStart,rayTarget,Color.red,0);
		
		return ray;
	}

	/*
	 *flip le gameObject lorsque le signe de sa vitesse change
	 */
	void Flip()
	{      
		facingRight = !facingRight;
		
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

}
