﻿using UnityEngine;
using System.Collections.Generic;

public class QTEhandler : MonoBehaviour {


	public WallMovement wall;
	public ButtonControl button1;
	public ButtonControl button2;

	public GameObject Player1;
	public GameObject Player2;

	public string inputP1 = "Fire1_P1";
	public string inputP2 = "Fire1_P2";

	public float QTETimerLimit = 0.5f;
	public int nbTap = 20;
	public int nbStep =10;

	public GameObject info1,info2;

	public AudioSource loopMusic;
	public AudioSource end;

	int p1Tap = 0;
	int p2Tap = 0;

	float timer = 0;
	bool QTEactive = false;
	bool QTEend = false;
	bool beginQTE = true;

	Vector3 p1savedPosition;
	Vector3 p2savedPosition;

	float savedGravity;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if (!QTEactive) 
		{
			if (button1.iSPush && button2.iSPush) 
			{
				QTEactive = true;
			}
		} else if (QTEactive && !QTEend) 
		{
			if(beginQTE)
			{
				loopMusic.Play();
				Player1.GetComponent<BetterPlayerController>().enabled = false;
				Player2.GetComponent<BetterPlayerController>().enabled = false;

				Player1.GetComponent<DashAbility>().enabled = false;
				Player2.GetComponent<DashAbility>().enabled = false;

				savedGravity = Player1.GetComponent<Rigidbody2D>().gravityScale;
				
				Player1.GetComponent<Rigidbody2D>().gravityScale = 0;
				Player2.GetComponent<Rigidbody2D>().gravityScale = 0;

				info1.SetActive(true);
				info2.SetActive(true);
				beginQTE = false;

				p1savedPosition = Player1.transform.position;
				p2savedPosition = Player2.transform.position;
			}

			Player1.transform.position = p1savedPosition;
			Player2.transform.position = p2savedPosition;

			if(Input.GetButtonDown(inputP1))
				p1Tap ++;

			if(Input.GetButtonDown(inputP2))
				p2Tap ++;

			if(p1Tap >= nbTap || p2Tap >= nbTap)
			{
				QTEend = true;

				if(p1Tap >= nbTap && p1Tap > p2Tap)
				{
					wall.wallMouvementRight(nbStep);
				}
				else if(p2Tap >= nbTap && p2Tap > p1Tap)
				{
					wall.wallMouvementLeft(nbStep);
				}
			}
		} else if (QTEactive && QTEend) 
		{
			loopMusic.Stop();
			end.Play ();

			p1Tap = p2Tap = 0;

			QTEactive = false;

			Player1.GetComponent<BetterPlayerController>().enabled = true;
			Player2.GetComponent<BetterPlayerController>().enabled = true;

			Player1.GetComponent<DashAbility>().enabled = true;
			Player2.GetComponent<DashAbility>().enabled = true;
			
			Player1.GetComponent<Rigidbody2D>().gravityScale = savedGravity;
			Player2.GetComponent<Rigidbody2D>().gravityScale = savedGravity;
			
			info1.SetActive(false);
			info2.SetActive(false);
			beginQTE = true;
			QTEend = false;
		}
	}
}
