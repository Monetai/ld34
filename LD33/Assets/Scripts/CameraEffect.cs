﻿using UnityEngine;
using System.Collections;

public class CameraEffect : MonoBehaviour {

	public enum Effect
	{
		zoomOnAction,
		none
	}

	Camera cam;
	Effect effect;


	public Transform zoomOn;
	public float animSpeed = 1f;
	public float animDuration = 2f;
	float timer = 0;

	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
		effect = Effect.none;
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch(effect)
		{
		case Effect.zoomOnAction:
				zoomOnaction();
			break;
		case Effect.none:
			break;
		}
	}

	public void playAnim(Effect anim)
	{
		effect = anim;
	}

	public void zoomOnaction()
	{
		timer += Time.deltaTime;

		if(timer < animDuration)
		{
			cam.orthographicSize = (Mathf.Lerp(cam.orthographicSize,3f, Time.deltaTime * animSpeed));
			cam.transform.position = Vector3.Lerp(cam.transform.position,
			                                      new Vector3(zoomOn.position.x,zoomOn.position.y,cam.transform.position.z),
			                                      Time.deltaTime* animSpeed);

			Time.timeScale = (Mathf.Lerp(Time.timeScale,0.3f, Time.deltaTime* animSpeed));
		}
		else
		{
			cam.orthographicSize = (Mathf.Lerp(cam.orthographicSize,5f, Time.deltaTime * animSpeed));
			cam.transform.position = Vector3.Lerp(cam.transform.position,
			                                      Vector3.zero,
			                                      Time.deltaTime* animSpeed);
			
			Time.timeScale = (Mathf.Lerp(Time.timeScale,1f, Time.deltaTime* animSpeed));

		}

		print (timer);
	}

}
