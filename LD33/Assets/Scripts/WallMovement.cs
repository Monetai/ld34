﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class WallMovement : MonoBehaviour {
	
	public Transform leftBound;
	public Transform rightBound;
	public int totalSteps = 100;
	public Image brother;
	public Image sister;
	public float speed =1f;

	public int deathTarget;
	public Canvas Menu;
	public BoxCollider2D boundLeft;
	public BoxCollider2D boundRight;

	Vector3 target;

	int leftStep = 0;
	
	int rightStep = 0;
	
	int currentStep;
	
	float distance;
	
	float step;

	// Music
	public AudioClip girlVictory,boyVictory;
	AudioSource ASgirlVictory,ASboyVictory;
	bool playMusic;

	public GameObject brotherWin;
	public GameObject sisterWin;
	// Use this for initialization
	void Start () {
		distance = Vector3.Distance(leftBound.position,rightBound.position);
		step = distance / totalSteps;
		currentStep = (int)(totalSteps/2);
		
		leftStep = rightStep = totalSteps - currentStep;
		this.transform.position = new Vector3(leftBound.position.x + currentStep *step,
		                                      this.transform.position.y,
		                                      this.transform.position.z);
		// Music
		playMusic = false;

		ASgirlVictory = gameObject.AddComponent<AudioSource>();
		ASgirlVictory.clip = girlVictory;
		ASgirlVictory.loop = true;

		ASboyVictory = gameObject.AddComponent<AudioSource>();
		ASboyVictory.clip = boyVictory;
		ASboyVictory.loop = true;
	}

	void Update () {
		if (currentStep <= deathTarget)  
		{
			leftBound.gameObject.SetActive(false);
			wallMouvementLeft(20);
			Menu.gameObject.SetActive(true);
			// Music
			playMusicGirl();
			sisterWin.SetActive(true);

		}

		if (currentStep >= totalSteps - deathTarget) 
		{
			rightBound.gameObject.SetActive(false);
			wallMouvementRight(20);
			Menu.gameObject.SetActive(true);
			// Music
			playMusicBoy();
			brotherWin.SetActive(true);

		}

		if (currentStep < 0 || currentStep > totalSteps) 
		{
			leftBound.gameObject.SetActive(false);
			rightBound.gameObject.SetActive(false);
			playMusic=false;
		}

	}

	// Update is called once per frame
	void FixedUpdate () {
		transform.position = Vector3.Lerp (transform.position,target,Time.fixedDeltaTime *speed);
	}
	
	public void wallMouvementRight(int nbSteps)
	{
		target = new Vector3(transform.position.x + (step * nbSteps), transform.position.y, transform.position.z);
		rightStep -= nbSteps;
		leftStep += nbSteps;
		currentStep += nbSteps;
		brother.fillAmount += 0.01f*nbSteps;
		sister.fillAmount-= 0.01f*nbSteps;
	}
	
	public void wallMouvementLeft(int nbSteps)
	{
		target = new Vector3(transform.position.x - (step * nbSteps), transform.position.y, transform.position.z);
		rightStep += nbSteps;
		leftStep -= nbSteps;
		currentStep -= nbSteps;
		brother.fillAmount -= 0.01f*nbSteps;
		sister.fillAmount += 0.01f*nbSteps;
	}

	// Music functions
	public void playMusicBoy(){
		if(!ASboyVictory.isPlaying){
			ASboyVictory.volume = 0.8f;
			ASboyVictory.Play ();
			playMusic=true;
		}
	}
	public void playMusicGirl(){
		if(!ASgirlVictory.isPlaying){
			ASgirlVictory.volume = 0.8f;
			ASgirlVictory.Play ();
			playMusic=true;
		}
	}
	// Getters
	public int getLeftStep(){
		return this.leftStep;
	}

	public int getRightStep(){
		return this.rightStep;
	}
}
