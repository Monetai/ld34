﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FillTextureManager : MonoBehaviour {

	Image img;
	public float amount;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image>();
		img.type = Image.Type.Filled;
	}
	
	// Update is called once per frame
	void Update () {
		img.fillAmount = amount;
	}
}
