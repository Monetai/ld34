﻿using UnityEngine;
using System.Collections.Generic;

public class ButtonWall : MonoBehaviour {


	public List<Transform> transformList;



	// Use this for initialization
	void Start () {
		moveToNextPos();

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void moveToNextPos()
	{
		this.gameObject.transform.position = transformList[Random.Range(0,transformList.Count)].position;
	}
}
