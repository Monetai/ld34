﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

	public WallMovement wall;

	// Son boy
	public AudioClip commonDrum,commonBass,boyDrum01,boyDrum02,boyHorn,boySteeldrum,boyTrumpet,boyUdu,boyVoices,boyWarriordrum;
	private AudioSource AScommonDrum,AScommonBass,ASboyDrum01,ASboyDrum02,ASboyHorn,ASboySteeldrum,ASboyTrumpet,ASboyUdu,ASboyVoices,ASboyWarriordrum;

	// Son girl
	public AudioClip girlDrum01,girlDrum02,girlbass,girlLead01,girlLead02,girlRise;
	private AudioSource ASgirlDrum01,ASgirlDrum02,ASgirlbass,ASgirlLead01,ASgirlLead02,ASgirlRise;

	public float maxVolume = 0.8f;
	//private float vASboyDrum01,vASboyDrum02,vASboyHorn,vASboySteeldrum,vASboyTrumpet,vASboyUdu,vASboyVoices,vASboyWarriordrum;
	private int bstep01 = 52;
	private int bstep01half = 57;
	private int bstep02 = 67;
	private int bstep03 = 75;

	private int gstep01 = 48;
	private int gstep01half = 43;
	private int gstep02 = 33;
	private int gstep03 = 25;

	private float fadeIn = 0.05f;
	private float fadeOut = 0.05f;

	// Use this for initialization
	void Start () {

		// common
		AScommonDrum = gameObject.AddComponent<AudioSource>();
		AScommonDrum.clip = commonDrum;
		AScommonDrum.loop = true;
		AScommonDrum.Play ();

		AScommonDrum.volume = maxVolume;

		AScommonBass = gameObject.AddComponent<AudioSource>();
		AScommonBass.clip = commonBass;
		AScommonBass.loop = true;
		AScommonBass.Play ();
		
		AScommonBass.volume = maxVolume;

		// boy sounds
		ASboyDrum01 = gameObject.AddComponent<AudioSource>();
		ASboyDrum01.clip = boyDrum01;
		ASboyDrum01.loop = true;
		ASboyDrum01.Play ();

		ASboyDrum02 = gameObject.AddComponent<AudioSource>();
		ASboyDrum02.clip = boyDrum02;
		ASboyDrum02.loop = true;
		ASboyDrum02.Play ();

		ASboyHorn = gameObject.AddComponent<AudioSource>();
		ASboyHorn.clip = boyHorn;
		ASboyHorn.loop = true;
		ASboyHorn.Play ();

		ASboySteeldrum = gameObject.AddComponent<AudioSource>();
		ASboySteeldrum.clip = boySteeldrum;
		ASboySteeldrum.loop = true;
		ASboySteeldrum.Play ();

		ASboyTrumpet = gameObject.AddComponent<AudioSource>();
		ASboyTrumpet.clip = boyTrumpet;
		ASboyTrumpet.loop = true;
		ASboyTrumpet.Play ();

		ASboyUdu = gameObject.AddComponent<AudioSource>();
		ASboyUdu.clip = boyUdu;
		ASboyUdu.loop = true;
		ASboyUdu.Play ();

		ASboyVoices = gameObject.AddComponent<AudioSource>();
		ASboyVoices.clip = boyVoices;
		ASboyVoices.loop = true;
		ASboyVoices.Play ();

		ASboyWarriordrum = gameObject.AddComponent<AudioSource>();
		ASboyWarriordrum.clip = boyWarriordrum;
		ASboyWarriordrum.loop = true;
		ASboyWarriordrum.Play ();

		// initialize volume
		ASboyDrum01.volume = 0;
		ASboyDrum02.volume = 0;
		ASboyHorn.volume = 0;
		ASboySteeldrum.volume = 0;
		ASboyTrumpet.volume = 0;
		ASboyUdu.volume = 0;
		ASboyVoices.volume = 0;
		ASboyWarriordrum.volume = 0;

		// girl sounds
		ASgirlDrum01 = gameObject.AddComponent<AudioSource>();
		ASgirlDrum01.clip = girlDrum01;
		ASgirlDrum01.loop = true;
		ASgirlDrum01.Play ();

		ASgirlDrum02 = gameObject.AddComponent<AudioSource>();
		ASgirlDrum02.clip = girlDrum02;
		ASgirlDrum02.loop = true;
		ASgirlDrum02.Play ();

		ASgirlbass = gameObject.AddComponent<AudioSource>();
		ASgirlbass.clip = girlbass;
		ASgirlbass.loop = true;
		ASgirlbass.Play ();

		ASgirlLead01 = gameObject.AddComponent<AudioSource>();
		ASgirlLead01.clip = girlLead01;
		ASgirlLead01.loop = true;
		ASgirlLead01.Play ();

		ASgirlLead02 = gameObject.AddComponent<AudioSource>();
		ASgirlLead02.clip = girlLead02;
		ASgirlLead02.loop = true;
		ASgirlLead02.Play ();

		ASgirlRise = gameObject.AddComponent<AudioSource>();
		ASgirlRise.clip = girlRise;
		ASgirlRise.loop = true;
		ASgirlRise.Play ();

		// initialize volume
		ASgirlDrum01.volume = 0;
		ASgirlDrum02.volume = 0;
		ASgirlbass.volume = 0;
		ASgirlLead01.volume = 0;
		ASgirlLead02.volume = 0;
		ASgirlRise.volume = 0;

	}
	
	// Update is called once per frame
	void Update () {

		print ("wallleft" + wall.getLeftStep ());
		print ("wallright" + wall.getRightStep ());

		if (wall.getLeftStep () >= 0 && wall.getLeftStep () <= 100) {
			// common drum ans bass
			if (wall.getLeftStep () >= 48 && wall.getLeftStep () < 52) {
				AScommonDrum.volume = maxVolume;
			} 
			if (!(wall.getLeftStep () >= 48 && wall.getLeftStep () < 52)) {
				AScommonDrum.volume = 0;
			}


			/****************************************************/
			/******************* BOY SOUNDS *********************/
			/****************************************************/

			// drum01
			if (wall.getLeftStep () >= bstep01 && wall.getLeftStep () < bstep03) {
				ASboyDrum01.volume = maxVolume;
			} 
			if (!(wall.getLeftStep () >= bstep01 && wall.getLeftStep () < bstep03)) {
				ASboyDrum01.volume = 0;
			}

			// drum02
			if (wall.getLeftStep () >= bstep03) {
				ASboyDrum02.volume = maxVolume;
			} 
			if (!(wall.getLeftStep () >= bstep03)) {
				ASboyDrum02.volume = 0;
			}

			// horn
			if (!(wall.getLeftStep () >= bstep01)) {
				if (ASboyHorn.volume >= 0) {
					ASboyHorn.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () >= bstep01) {
				if (ASboyHorn.volume < maxVolume) {
					ASboyHorn.volume += fadeIn;
				}
			}

			// steel drum
			if (wall.getLeftStep () < bstep01half) {
				if (ASboySteeldrum.volume >= 0) {
					ASboySteeldrum.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () >= bstep03) {
				if (ASboySteeldrum.volume >= 0) {
					ASboySteeldrum.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () >= bstep01half && wall.getLeftStep () < bstep03) {
				if (ASboySteeldrum.volume < maxVolume) {
					ASboySteeldrum.volume += fadeIn;
				}
			}

			// voices
			if (wall.getLeftStep () < bstep03) {
				if (ASboyVoices.volume >= 0) {
					ASboyVoices.volume -= fadeOut / 2;
				}
			}
			if (wall.getLeftStep () >= bstep03) {
				if (ASboyVoices.volume < maxVolume) {
					ASboyVoices.volume += fadeIn / 2;
				}
			}

			// trumpet
			if (wall.getLeftStep () < bstep03) {
				if (ASboyTrumpet.volume >= 0) {
					ASboyTrumpet.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () >= bstep03) {
				if (ASboyTrumpet.volume < maxVolume - 0.5f) {
					ASboyTrumpet.volume += fadeIn / 2;
				}
			}

			// warrior
			if (wall.getLeftStep () < bstep03) {
				if (ASboyWarriordrum.volume >= 0) {
					ASboyWarriordrum.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () >= bstep03) {
				if (ASboyWarriordrum.volume < maxVolume) {
					ASboyWarriordrum.volume += fadeIn / 2;
				}
			}


			/****************************************************/
			/******************* GIRL SOUNDS ********************/
			/****************************************************/

			// drum01
			if (wall.getLeftStep () <= gstep01 && wall.getLeftStep () > gstep02) {
				ASgirlDrum01.volume = maxVolume;
			} 
			if (!(wall.getLeftStep () <= gstep01 && wall.getLeftStep () > gstep02)) {
				ASgirlDrum01.volume = 0;
			}

			// drum02
			if (wall.getLeftStep () <= gstep02) {
				ASgirlDrum02.volume = maxVolume;
			} 
			if (!(wall.getLeftStep () <= gstep02)) {
				ASgirlDrum02.volume = 0;
			}

			// bass
			if (!(wall.getLeftStep () <= gstep01)) {
				if (ASgirlbass.volume >= 0) {
					ASgirlbass.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () <= gstep01) {
				if (ASgirlbass.volume < maxVolume) {
					ASgirlbass.volume += fadeIn;
				}
			}

			// Lead01
			if (!(wall.getLeftStep () <= gstep01half)) {
				if (ASgirlLead01.volume >= 0) {
					ASgirlLead01.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () <= gstep01half) {
				if (ASgirlLead01.volume < maxVolume) {
					ASgirlLead01.volume += fadeIn;
				}
			}

			// Lead02
			if (!(wall.getLeftStep () <= gstep03)) {
				if (ASgirlLead02.volume >= 0) {
					ASgirlLead02.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () <= gstep03) {
				if (ASgirlLead02.volume < maxVolume) {
					ASgirlLead02.volume += fadeIn;
				}
			}

			// Rise
			if (!(wall.getLeftStep () <= gstep03)) {
				if (ASgirlRise.volume >= 0) {
					ASgirlRise.volume -= fadeOut;
				}
			}
			if (wall.getLeftStep () <= gstep03) {
				if (ASgirlRise.volume < maxVolume) {
					ASgirlRise.volume += 0.01f;
				}
			}
		} else {
			maxVolume=0;
			print("coucou");
			AScommonDrum.volume=0;
			AScommonBass.volume=0;
			ASboyDrum01.volume=0;
			ASboyDrum02.volume=0;
			ASboyHorn.volume=0;
			ASboySteeldrum.volume=0;
			ASboyTrumpet.volume=0;
			ASboyUdu.volume=0;
			ASboyVoices.volume=0;
			ASboyWarriordrum.volume=0;
			ASgirlDrum01.volume=0;
			ASgirlDrum02.volume=0;
			ASgirlbass.volume=0;
			ASgirlLead01.volume=0;
			ASgirlLead02.volume=0;
			ASgirlRise.volume=0;
		}
	}
}