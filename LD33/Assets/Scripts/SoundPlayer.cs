﻿using UnityEngine;
using System.Collections.Generic;

public class SoundPlayer : MonoBehaviour {

	public List<AudioClip> stepSounds;
	public List<AudioClip> jumpSounds;
	public List<AudioClip> punchSounds;

	AudioSource source;

    // Use this for initialization
    void Start () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	private AudioClip getRandomSound(List<AudioClip> list)
    {
        int randomSound = Random.Range(1, list.Count-1);
		AudioClip soundToReturn = list[randomSound];

		list[randomSound] = list[0];
        list[0] = soundToReturn;

        return soundToReturn;
    }

	public void chooseStepSoundsAndPlay()
    {
		AudioClip soundToPlay;	
		soundToPlay = getRandomSound (stepSounds);

		//source.pitch = Random.Range (0.9f, 1.1f);
		source.PlayOneShot (soundToPlay);
    }

	public void chooseJumpSoundsAndPlay()
	{
		AudioClip soundToPlay;	
		soundToPlay = getRandomSound (jumpSounds);
		
		source.pitch = Random.Range (0.9f, 1.1f);
		source.PlayOneShot (soundToPlay);
	}

	public void choosePunchSoundsAndPlay()
	{
		AudioClip soundToPlay;	
		soundToPlay = getRandomSound (punchSounds);
		
		source.pitch = Random.Range (0.9f, 1.1f);
		source.PlayOneShot (soundToPlay);
	}
}